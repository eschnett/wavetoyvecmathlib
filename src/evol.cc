#include <vecmathlib.h>         // Include this before <cctk.h>
using namespace vecmathlib;

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cmath>
using namespace std;



namespace WTV {

// Determine good vector type
#if CCTK_REAL_PRECISION == 8
typedef float64_vec realvec_t;
#elif CCTK_REAL_PRECISION == 4
typedef float32_vec realvec_t;
#else
typedef realvec<CCTK_REAL,1> realvec_t;
#endif
// Determine matching other vector and scalar types
typedef realvec_t::intvec_t intvec_t;
typedef realvec_t::boolvec_t boolvec_t;
typedef realvec_t::real_t real_t;
typedef realvec_t::int_t int_t;
typedef realvec_t::uint_t uint_t;
// bool_t is always bool
const int vecsize = realvec_t::size;
typedef vecmathlib::mask_t<realvec_t> mask_t;



// Note: The "loadu" and "storeu" calls below assume unaligned access.
// This is reasonably efficient only on Intel architectures. When
// aligning and/or padding grid functions, faster aligned load/store
// operations could be used.

inline
void evol(const real_t* restrict phi_p_p,
          const real_t* restrict phi_p,
          real_t *restrict phi,
          int_t dj, int_t dk,
          real_t dt2, real_t idx2, real_t idy2, real_t idz2,
          mask_t m)
{
  DECLARE_CCTK_PARAMETERS;
  realvec_t phiv_p_p = realvec_t::loadu(phi_p_p);
  realvec_t phiv_p = realvec_t::loadu(phi_p);
  // ddphi/dx/dx
  realvec_t phiv_p_xm = realvec_t::loadu(phi_p-1);
  realvec_t phiv_p_xp = realvec_t::loadu(phi_p+1);
  realvec_t dxx_phi =
    (phiv_p_xm - realvec_t(2.0) * phiv_p +  phiv_p_xp) * realvec_t(idx2);
  // ddphi/dy/dy
  realvec_t phiv_p_ym = realvec_t::loadu(phi_p-dj);
  realvec_t phiv_p_yp = realvec_t::loadu(phi_p+dj);
  realvec_t dyy_phi = 
    (phiv_p_ym - realvec_t(2.0) * phiv_p +  phiv_p_yp) * realvec_t(idy2);
  // ddphi/dz/dz
  realvec_t phiv_p_zm = realvec_t::loadu(phi_p-dk);
  realvec_t phiv_p_zp = realvec_t::loadu(phi_p+dk);
  realvec_t dzz_phi =
    (phiv_p_zm - realvec_t(2.0) * phiv_p +  phiv_p_zp) * realvec_t(idz2);
  // ddphi/dt/dt
  realvec_t dtt_phi = dxx_phi + dyy_phi + dzz_phi;
  realvec_t phiv =
    realvec_t(2.0) * phiv_p - phiv_p_p + realvec_t(dt2) * dtt_phi;
  storeu(phiv, phi, m);
}



extern "C" void WTV_Evol(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  int_t dj = CCTK_GFINDEX3D(cctkGH, 0,1,0) - CCTK_GFINDEX3D(cctkGH, 0,0,0);
  int_t dk = CCTK_GFINDEX3D(cctkGH, 0,0,1) - CCTK_GFINDEX3D(cctkGH, 0,0,0);
  real_t dt2 = pow(CCTK_DELTA_TIME, 2);
  real_t idx2 = 1.0 / pow(CCTK_DELTA_SPACE(0), 2);
  real_t idy2 = 1.0 / pow(CCTK_DELTA_SPACE(1), 2);
  real_t idz2 = 1.0 / pow(CCTK_DELTA_SPACE(2), 2);
  
#pragma omp parallel
  CCTK_LOOP3STR_ALL(WTV_Evol, cctkGH, i,j,k, imin,imax, vecsize) {
    int_t ind = CCTK_GFINDEX3D(cctkGH, i,j,k);
    mask_t m(i, imin, imax, 0);
    evol(&phi_p_p[ind], &phi_p[ind], &phi[ind],
         dj, dk, dt2, idx2, idy2, idz2, m);
  } CCTK_ENDLOOP3STR_ALL(WTV_Evol);
}

}
