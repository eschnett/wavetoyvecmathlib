#include <vecmathlib.h>         // Include this before <cctk.h>
using namespace vecmathlib;

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cmath>
using namespace std;



namespace WTV {

// Determine good vector type
#if CCTK_REAL_PRECISION == 8
typedef float64_vec realvec_t;
#elif CCTK_REAL_PRECISION == 4
typedef float32_vec realvec_t;
#else
typedef realvec<CCTK_REAL,1> realvec_t;
#endif
// Determine matching other vector and scalar types
typedef realvec_t::intvec_t intvec_t;
typedef realvec_t::boolvec_t boolvec_t;
typedef realvec_t::real_t real_t;
typedef realvec_t::int_t int_t;
typedef realvec_t::uint_t uint_t;
// bool_t is always bool
const int vecsize = realvec_t::size;
typedef vecmathlib::mask_t<realvec_t> mask_t;



inline
void init(real_t t, real_t y, real_t z,
          const real_t* restrict x,
          real_t *restrict phi,
          mask_t m)
{
  DECLARE_CCTK_PARAMETERS;
  real_t pi = M_PI;
  real_t omega = sqrt(pow(kx,2) + pow(ky,2) + pow(kz,2));
  real_t phi1 = amp * cos(2*pi*omega*t) * sin(2*pi*ky*y) * sin(2*pi*kz*z);
  realvec_t xv = realvec_t::loadu(x);
  realvec_t phiv = realvec_t(phi1) * sin(realvec_t(2*pi*kx) * x);
  storeu(phiv, phi, m);
}

extern "C" void WTV_Init(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
#pragma omp parallel
  CCTK_LOOP3STR_ALL(WTV_Init, cctkGH, i,j,k, imin,imax, vecsize) {
    int_t ind = CCTK_GFINDEX3D(cctkGH, i,j,k);
    mask_t m(i, imin, imax, 0);
    init(cctk_time, y[ind], z[ind], &x[ind], &phi[ind], m);
  } CCTK_ENDLOOP3STR_ALL(WTV_Init);
}

}
