#include <vecmathlib.h>         // Include this before <cctk.h>
using namespace vecmathlib;

#include <cctk.h>
#include <cctk_Arguments.h>
#include <cctk_Parameters.h>

#include <cmath>
using namespace std;



namespace WTV {

// Determine good vector type
#if CCTK_REAL_PRECISION == 8
typedef float64_vec realvec_t;
#elif CCTK_REAL_PRECISION == 4
typedef float32_vec realvec_t;
#else
typedef realvec<CCTK_REAL,1> realvec_t;
#endif
// Determine matching other vector and scalar types
typedef realvec_t::intvec_t intvec_t;
typedef realvec_t::boolvec_t boolvec_t;
typedef realvec_t::real_t real_t;
typedef realvec_t::int_t int_t;
typedef realvec_t::uint_t uint_t;
// bool_t is always bool
const int vecsize = realvec_t::size;
typedef vecmathlib::mask_t<realvec_t> mask_t;



inline
void energy(const real_t* restrict phi_p_p,
            const real_t* restrict phi_p,
            const real_t* restrict phi,
            real_t *restrict eps,
            int_t dj, int_t dk,
            real_t idt, real_t idx, real_t idy, real_t idz,
            mask_t m)
{
  DECLARE_CCTK_PARAMETERS;
  // dphi/dx
  realvec_t phiv_xm = realvec_t::loadu(phi-1);
  realvec_t phiv_xp = realvec_t::loadu(phi+1);
  realvec_t dx_phi = (phiv_xp - phiv_xm) * realvec_t(idx);
  // dphi/dy
  realvec_t phiv_ym = realvec_t::loadu(phi-dj);
  realvec_t phiv_yp = realvec_t::loadu(phi+dj);
  realvec_t dy_phi = (phiv_yp - phiv_ym) * realvec_t(idy);
  // dphi/dz
  realvec_t phiv_zm = realvec_t::loadu(phi-dk);
  realvec_t phiv_zp = realvec_t::loadu(phi+dk);
  realvec_t dz_phi = (phiv_zp - phiv_zm) * realvec_t(idz);
  // dphi/dt
  realvec_t phiv = realvec_t::loadu(phi);
  realvec_t phiv_p = realvec_t::loadu(phi_p);
  realvec_t phiv_p_p = realvec_t::loadu(phi_p_p);
  realvec_t dt_phi =
    (+ realvec_t(1.5) * phiv 
     - realvec_t(2.0) * phiv_p
     + realvec_t(0.5) * phiv_p_p) * realvec_t(idt);
  // eps
  realvec_t epsv =
    (realvec_t(0.5) *
     (dt_phi*dt_phi + dx_phi*dx_phi + dy_phi*dy_phi + dz_phi*dz_phi));
  storeu(epsv, eps, m);
}



extern "C" void WTV_Energy(CCTK_ARGUMENTS)
{
  DECLARE_CCTK_ARGUMENTS;
  DECLARE_CCTK_PARAMETERS;
  
  int_t dj = CCTK_GFINDEX3D(cctkGH, 0,1,0) - CCTK_GFINDEX3D(cctkGH, 0,0,0);
  int_t dk = CCTK_GFINDEX3D(cctkGH, 0,0,1) - CCTK_GFINDEX3D(cctkGH, 0,0,0);
  real_t idt = 1.0 / CCTK_DELTA_TIME;
  real_t idx = 1.0 / CCTK_DELTA_SPACE(0);
  real_t idy = 1.0 / CCTK_DELTA_SPACE(1);
  real_t idz = 1.0 / CCTK_DELTA_SPACE(2);
  
#pragma omp parallel
  CCTK_LOOP3STR_ALL(WTV_Energy, cctkGH, i,j,k, imin,imax, vecsize) {
    int_t ind = CCTK_GFINDEX3D(cctkGH, i,j,k);
    mask_t m(i, imin, imax, 0);
    energy(&phi_p_p[ind], &phi_p[ind], &phi[ind], &eps[ind],
           dj, dk, idt, idx, idy, idz, m);
  } CCTK_ENDLOOP3STR_ALL(WTV_Energy);
}

}
